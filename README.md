# UTN DevOps - Grupo 7


Repository created by Group 7 of the UTN 'DevOps' course.

**Members:

- Avigliano, Camila Lujan
- Bertolini, Uriel
- Betancourt Oliva, Luis
- Ferravante, Marcelo
- Mackevich, Romina
- Navarta, Ezequiel
- Perotti, Julian
- Ramirez, Andrea
- Vurela, Edgardo
- Liendo, Juan Pablo**
